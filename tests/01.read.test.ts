import { InMemoryStore } from "../src/inMemoryStore";
import { expect } from "chai";

describe("read", () => {
  const store = new InMemoryStore({
    foo: {
      bar: 42,
    },
  });

  it("reads data at specified path", async () => {
    expect(await store.read("foo.bar")).to.eq(42);
  });

  it("reads undefined if data is missing", async () => {
    expect(await store.read("foo.bar.baz.a.b.c")).to.be.undefined;
  });

  it("reads all children values", async () => {
    expect(await store.read("foo")).to.deep.eq({
      bar: 42,
    });
  });

  it("ignores repeated dots", async () => {
    expect(await store.read("foo...bar")).to.eq(42);
  });

  it("reads full data for root path", async () => {
    expect(await store.read(".")).to.deep.eq({
      foo: {
        bar: 42,
      },
    });
  });

  it("doesn't read Object prototype properties", async () => {
    expect(await store.read("toString")).to.be.undefined;
  });
});
