import { InMemoryStore } from "../src/inMemoryStore";
import { expect } from "chai";
import { stub, assert } from "sinon";

describe("observe", () => {
  let store: InMemoryStore;

  beforeEach(() => {
    store = new InMemoryStore();
  });

  it("triggers callback for exact path", async () => {
    const callback = stub();
    await store.observe("foo.bar", callback);

    assert.calledWithExactly(callback, undefined);

    await store.write("foo.bar", 42);

    assert.calledWithExactly(callback, 42);
  });

  it("triggers callback for parent path", async () => {
    const callback = stub();
    await store.observe("foo", callback);

    assert.calledWithExactly(callback, undefined);

    await store.write("foo.bar", 42);

    assert.calledWithExactly(callback, {
      bar: 42,
    });
  });

  it("triggers callback for root path", async () => {
    const callback = stub();
    await store.observe(".", callback);

    assert.calledWithExactly(callback, {});

    await store.write("foo.bar", 42);

    assert.calledWithExactly(callback, {
      foo: {
        bar: 42,
      },
    });
  });

  it("triggers multiple callbacks for different paths", async () => {
    const fooCallback = stub();
    const fooBarCallback = stub();
    const fooBazCallback = stub();
    await store.observe("foo", fooCallback);
    await store.observe("foo.bar", fooBarCallback);
    await store.observe("foo.baz", fooBazCallback);

    assert.calledWithExactly(fooCallback, undefined);
    assert.calledWithExactly(fooBarCallback, undefined);
    assert.calledWithExactly(fooBazCallback, undefined);

    await store.write("foo.bar", 42);

    assert.calledWithExactly(fooCallback, {
      bar: 42,
    });
    assert.calledWithExactly(fooBarCallback, 42);
    assert.notCalled(fooBazCallback);
  });
});
