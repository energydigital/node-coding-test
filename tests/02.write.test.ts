import { InMemoryStore } from "../src/inMemoryStore";
import { expect } from "chai";

describe("write", () => {
  let store: InMemoryStore;

  beforeEach(() => {
    store = new InMemoryStore();
  });

  it("updates value", async () => {
    await store.write("foo.bar", 42);

    expect(await store.read("foo.bar")).to.eq(42);
  });

  it("updates object value", async () => {
    await store.write("foo", { bar: 42 });

    expect(await store.read("foo.bar")).to.eq(42);
  });

  it("ignores repeated dots", async () => {
    await store.write(".foo..bar...", 42);

    expect(await store.read("foo...bar.")).to.eq(42);
  });

  it("works with multiple properties", async () => {
    await store.write("foo.bar.a", "a value");
    await store.write("foo.bar.b", "b value");

    expect(await store.read("foo.bar.a")).to.eq("a value");
    expect(await store.read("foo.bar.b")).to.eq("b value");
  });

  it("overrides primitive value if needed", async () => {
    await store.write("foo.bar", 42);
    await store.write("foo.bar.baz", "test");

    expect(await store.read("foo.bar")).to.deep.eq({
      baz: "test",
    });
  });

  it(`doesn't allow to update root value`, async () => {
    try {
      await store.write(".", 42);
      expect.fail();
    } catch (err) {
      expect(err.message).to.eq("Cannot update root value");
    }
  });
});
