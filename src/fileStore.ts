import { DataStore, StoredValue } from "./dataStore";
import { InMemoryStore } from "./inMemoryStore";

export class FileStore extends InMemoryStore implements DataStore {
  constructor(filePath: string) {
    super();
  }

  public async write(path: string, value: StoredValue): Promise<void> {
    throw new Error("Not implemented yet");
  }
}
